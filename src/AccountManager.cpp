/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AccountManager.h"
#include "BCrypt.h"
#include "DatabaseManager.h"
#include "litelogcpp/LiteLog.h"
#include <bsoncxx/builder/stream/document.hpp>
#include <fstream>
#include <mongocxx/result/insert_one.hpp>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <base64.h>

AccountManager::ErrorCode AccountManager::createAccountFromJSON(const sajson::value& jsonRoot)
{
    sajson::value username = jsonRoot.get_value_of_key(sajson::literal("username"));
    sajson::value email = jsonRoot.get_value_of_key(sajson::literal("email"));
    sajson::value password = jsonRoot.get_value_of_key(sajson::literal("password"));

    std::string pwString;
    std::string emailString;
    if (username.get_type() != sajson::TYPE_STRING || email.get_type() != sajson::TYPE_STRING
        || password.get_type() != sajson::TYPE_STRING) {
        return ErrorCode::JSON_INVALID;
    }
    const std::string usernameString = username.as_string();
    if ((pwString = password.as_string()).size() < 16 || pwString.size() > 72) {
        return ErrorCode::PASSWORD_INVALID;
    }
    if ((emailString = email.as_string()).size() < 3 || emailString.size() > 72) {
        return ErrorCode::EMAIL_INVALID;
    }
    std::array<unsigned char, EVP_MAX_MD_SIZE> hashedEmail{};
    if (!EVP_Digest(emailString.data(), emailString.size(), hashedEmail.data(), nullptr, EVP_sha512(), nullptr)) {
        LiteLog::log<LiteLog::ERROR>(std::string("Error when trying to digest the user email with OpenSSL! - Error: ")
                                         .append(ERR_error_string(ERR_get_error(), nullptr)));
        return ErrorCode::OPENSSL_DIGEST_FAILED;
    }

    bsoncxx::types::b_binary hashedEmailDBRepresentation{.size = hashedEmail.size(), .bytes = hashedEmail.data()};

    using bsoncxx::builder::stream::close_array;
    using bsoncxx::builder::stream::close_document;
    using bsoncxx::builder::stream::document;
    using bsoncxx::builder::stream::finalize;
    using bsoncxx::builder::stream::open_array;
    using bsoncxx::builder::stream::open_document;

    // Looks if there's already an account in DB with either same email *or* username
    // The filter document looks like this
    // $or: [{"username": usernameString},
    //      {"email": hashedEmailDBRepresentation}]
    if (DatabaseManager::db["accounts"].find_one(document{} << "$or" << open_array << open_document << "username"
                                                            << usernameString << close_document << open_document
                                                            << "email" << hashedEmailDBRepresentation << close_document
                                                            << close_array << finalize)) {
        return ErrorCode::ACCOUNT_ALREADY_EXISTS;
    }

    bsoncxx::document::value documentValue = document{} << "username" << usernameString << "email"
                                                        << hashedEmailDBRepresentation << "password"
                                                        << BCrypt::hashPassword(pwString) << finalize;
    bsoncxx::stdx::optional<mongocxx::result::insert_one> queryResult =
        DatabaseManager::db["accounts"].insert_one(documentValue.view());
    return ErrorCode::SUCCESS;
}
AccountManager::LoginDetails AccountManager::checkLogin(const sajson::value& jsonRoot)
{
    sajson::value email = jsonRoot.get_value_of_key(sajson::literal("email"));
    sajson::value password = jsonRoot.get_value_of_key(sajson::literal("password"));

    std::string pwString;
    std::string emailString;
    if (email.get_type() != sajson::TYPE_STRING || password.get_type() != sajson::TYPE_STRING) {
        return {.returnCode = ErrorCode::JSON_INVALID};
    }
    if ((pwString = password.as_string()).size() < 16 || pwString.size() > 72) {
        return {.returnCode = ErrorCode::PASSWORD_INVALID};
    }
    if ((emailString = email.as_string()).size() < 3 || emailString.size() > 72) {
        return {.returnCode = ErrorCode::EMAIL_INVALID};
    }
    std::array<unsigned char, EVP_MAX_MD_SIZE> hashedEmail{};
    if (!EVP_Digest(emailString.data(), emailString.size(), hashedEmail.data(), nullptr, EVP_sha512(), nullptr)) {
        LiteLog::log<LiteLog::ERROR>(std::string("Error when trying to digest the user email with OpenSSL! - Error: ")
                                         .append(ERR_error_string(ERR_get_error(), nullptr)));
        return {.returnCode = ErrorCode::OPENSSL_DIGEST_FAILED};
    }
    auto builder = bsoncxx::builder::stream::document{};

    using bsoncxx::builder::stream::close_document;
    using bsoncxx::builder::stream::document;
    using bsoncxx::builder::stream::finalize;
    using bsoncxx::builder::stream::open_document;
    using bsoncxx::builder::stream::open_array;
    using bsoncxx::builder::stream::close_array;

    // TODO: 2FA Authentication
    // TODO: Token expiry
    // TODO: Email login notification
    // TODO: Registration email verification
    std::ifstream randomFile;
    randomFile.open("/dev/random");
    std::string buffer(128, '\0');
    buffer.reserve(140);
    randomFile.read(buffer.data(), 128);
    // Stream base64 encoding
    buffer = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(
                                std::chrono::system_clock::now().time_since_epoch()).count()) + ":" + base64_encode(buffer);
    boost::optional<mongocxx::result::update> account = DatabaseManager::db["accounts"].update_one(
        document{} << "email" << bsoncxx::types::b_binary{.size = hashedEmail.size(), .bytes = hashedEmail.data()}
                   << finalize,
        document{} << "$addToSet" << open_document << "loginTokens"
                   << open_document << buffer << open_array << close_array << close_document
                   << close_document << finalize);

    if (account->matched_count() < 1) {
        return {.returnCode = ErrorCode::ACCOUNT_DOESNT_EXIST};
    }
    return {.returnCode = ErrorCode::SUCCESS, .token = std::move(buffer)};
}
