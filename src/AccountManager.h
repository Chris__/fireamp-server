/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FIREAMPSERVER_ACCOUNTMANAGER_H
#define FIREAMPSERVER_ACCOUNTMANAGER_H

#include "sajson.h"

namespace AccountManager
{
    enum ErrorCode {
        SUCCESS,
        JSON_INVALID,
        PASSWORD_INVALID,
        EMAIL_INVALID,
        ACCOUNT_ALREADY_EXISTS,
        ACCOUNT_DOESNT_EXIST,
        OPENSSL_DIGEST_FAILED
    };
    struct LoginDetails {
        ErrorCode returnCode;
        std::string token;
    };
    ErrorCode createAccountFromJSON(const sajson::value& jsonRoot);
    LoginDetails checkLogin(const sajson::value& jsonRoot);
}

#endif // FIREAMPSERVER_ACCOUNTMANAGER_H
