/*
 * Copyright (C) 2022 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bsoncxx/stdx/optional.hpp>
#include <mongocxx/result/insert_one.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include "DatabaseManager.h"
#include "ChatManager.h"

std::string ChatManager::createChatFromJSON(const sajson::value& jsonRoot)
{
    sajson::value chatPartners = jsonRoot.get_value_of_key(sajson::literal("chatpartners"));
    if (chatPartners.get_type() != sajson::TYPE_ARRAY || chatPartners.get_length() < 1 || chatPartners.get_length() > 220) {
        return "";
    }

    using bsoncxx::builder::basic::kvp;
    using bsoncxx::builder::basic::sub_array;

    auto documentBuilder = bsoncxx::builder::basic::document{};
    documentBuilder.append(kvp("members", [&chatPartners](sub_array child) {
        for (int i = 0; i < chatPartners.get_length(); i++) {
            sajson::value chatPartner = chatPartners.get_array_element(i);
            if (chatPartner.get_type() != sajson::TYPE_STRING) {
                return "";
                // TODO: ERROR HANDLING
            }
            child.append(chatPartner.as_string());
        }
    }));

    bsoncxx::stdx::optional<mongocxx::result::insert_one> result =
        DatabaseManager::Collections::chats.insert_one(documentBuilder.view());
    return "";
}
