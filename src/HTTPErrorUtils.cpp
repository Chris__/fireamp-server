/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTTPErrorUtils.h"
#include "vulcanohttp/HTTPResponse.h"

void HTTPErrorUtils::sendMethodNotAllowed(HTTPServer& httpServer,
                                                   const HTTPRequest& request,
                                                   const std::string& allowedMethods)
{
    using HTTP::FrameUtils::HTTPHeaderField;
    using HTTP::FrameUtils::INDEXED_LITERAL;

    HTTPResponse(httpServer,
                 request,
                 {{INDEXED_LITERAL, 10, "405"}, // Show that method is not allowed
                  {INDEXED_LITERAL, 22, allowedMethods}, // Show allowed methods
                  {INDEXED_LITERAL, 54, "fireamp-server"}})
        .send();
}

void HTTPErrorUtils::sendErrorResponse(HTTPServer& httpServer,
                                                const HTTPRequest& request,
                                                const std::string& responseCode,
                                                const std::string& errorMessage)
{
    using HTTP::FrameUtils::HTTPHeaderField;
    using HTTP::FrameUtils::INDEXED_LITERAL;

    HTTPResponse(httpServer,
                 request,
                 {{INDEXED_LITERAL, 10, responseCode},
                  {INDEXED_LITERAL, 54, "fireamp-server"},
                  {INDEXED_LITERAL, 31, "text/plain;charset=utf-8"}},
                 errorMessage)
        .send();
}