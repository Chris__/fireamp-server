/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FIREAMPSERVER_BCRYPT_H
#define FIREAMPSERVER_BCRYPT_H

#include <string>

namespace BCrypt
{
    std::string hashPassword(const std::string& pw);
    bool validatePassword(const std::string& pw, const std::string& hashedPW);
};

#endif // FIREAMPSERVER_BCRYPT_H
