/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AccountManager.h"
#include "DatabaseManager.h"
#include "ChatManager.h"
#include "HTTPErrorUtils.h"
#include "vulcanohttp/HTTPResponse.h"
#include <iostream>
#include <litelogcpp/LiteLog.h>
#include <sajson.h>
#include <vulcanohttp/HTTPServer.h>

HTTPServer fireampHttpServer("test-certificate.pem", "test-privatekey.pem", 32452);

void handleRegisterEndpoint(const HTTPRequest& request)
{
    if (request.getHeaderFieldFromKey(":method").second != "POST") {
        HTTPErrorUtils::sendMethodNotAllowed(fireampHttpServer, request, "POST");
        return;
    } else if (request.bodyData.empty()) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "POST requests to this endpoint need a request body");
        return;
    } else if (request.getHeaderFieldFromKey("content-type").second != "application/json;charset=utf-8") {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer,
            request,
            "415",
            "The server doesn't accept the data format of the body given in the Content-Type field. It requires JSON");
        return;
    }

    std::string result;
    result.reserve(request.bodyData.size());

    for (auto const& c : request.bodyData) {
        result += c;
    }
    const sajson::value& root =
        sajson::parse(sajson::dynamic_allocation(), sajson::mutable_string_view(request.bodyData.size(), result.data()))
            .get_root();

    if (root.get_type() != sajson::TYPE_OBJECT) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "You didn't send a valid registration JSON!");
        return;
    }
    switch (AccountManager::createAccountFromJSON(root)) {
        using namespace std::string_literals;
    case AccountManager::SUCCESS:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{8}}, R"({code: 0, text: "Success! Your account was created!"})"s).send();
        break;
    case AccountManager::JSON_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 1, text: "JSON invalid"})"s).send();
        break;
    case AccountManager::PASSWORD_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 2, text: "Sorry, the password you entered doesn't meet the requirements!"})"s).send();
        break;
    case AccountManager::EMAIL_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 3, text: "Sorry, the email you entered isn't valid. It's either to long or short or not an email address"})"s).send();
        break;
    case AccountManager::ACCOUNT_ALREADY_EXISTS:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 4, text: "Sorry, there is already an account with this username or email"})"s).send();
        break;
    case AccountManager::OPENSSL_DIGEST_FAILED:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{14}}, R"({code: 5, text: "Server error! Please report this to the owners of this instance"})"s).send();
        break;
    }
}
void handleLoginEndpoint(const HTTPRequest& request)
{
    if (request.getHeaderFieldFromKey(":method").second != "POST") {
        HTTPErrorUtils::sendMethodNotAllowed(fireampHttpServer, request, "POST");
        return;
    } else if (request.bodyData.empty()) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "POST requests to this endpoint need a request body");
        return;
    } else if (request.getHeaderFieldFromKey("content-type").second != "application/json;charset=utf-8") {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer,
            request,
            "415",
            "The server doesn't accept the data format of the body given in the Content-Type field. It requires JSON");
        return;
    }

    std::string result;
    result.reserve(request.bodyData.size());

    for (auto const& c : request.bodyData) {
        result += c;
    }
    const sajson::value& root =
        sajson::parse(sajson::dynamic_allocation(), sajson::mutable_string_view(request.bodyData.size(), result.data()))
            .get_root();

    if (root.get_type() != sajson::TYPE_OBJECT) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "You didn't send a valid registration JSON!");
        return;
    }
    AccountManager::LoginDetails loginDetails = AccountManager::checkLogin(root);
    switch (loginDetails.returnCode) {
        using namespace std::string_literals;
    case AccountManager::SUCCESS:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{8}}, R"({code: 0, text: "Success! Your account was created!", token: ")"s + loginDetails.token + "\"}").send();
        break;
    case AccountManager::JSON_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 1, text: "JSON invalid"})"s).send();
        break;
    case AccountManager::PASSWORD_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 2, text: "Sorry, the password you entered doesn't meet the requirements!"})"s).send();
        break;
    case AccountManager::EMAIL_INVALID:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{12}},R"({code: 3, text: "Sorry, the email you entered isn't valid. It's either to long or short or not an email address"})"s).send();
        break;
    case AccountManager::ACCOUNT_DOESNT_EXIST:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{14}}, R"({code: 4, text: "This account doesn't exist!"})"s).send();
        break;
    case AccountManager::OPENSSL_DIGEST_FAILED:
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{14}}, R"({code: 5, text: "Server error! Please report this to the owners of this instance"})"s).send();
        break;
    }
}
void handleChatCreateEndpoint(const HTTPRequest& request)
{
    if (request.getHeaderFieldFromKey(":method").second != "POST") {
        HTTPErrorUtils::sendMethodNotAllowed(fireampHttpServer, request, "POST");
        return;
    } else if (request.bodyData.empty()) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "POST requests to this endpoint need a request body");
        return;
    } else if (request.getHeaderFieldFromKey("content-type").second != "application/json;charset=utf-8") {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer,
            request,
            "415",
            "The server doesn't accept the data format of the body given in the Content-Type field. It requires JSON");
        return;
    }

    std::string result;
    result.reserve(request.bodyData.size());

    for (auto const& c : request.bodyData) {
        result += c;
    }
    const sajson::value& root =
        sajson::parse(sajson::dynamic_allocation(), sajson::mutable_string_view(request.bodyData.size(), result.data()))
            .get_root();

    if (root.get_type() != sajson::TYPE_OBJECT) {
        HTTPErrorUtils::sendErrorResponse(
            fireampHttpServer, request, "400", "You didn't send a valid registration JSON!");
        return;
    }
    ChatManager::createChatFromJSON(root);
    using namespace std::string_literals;
    HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{8}}, R"({code: 0, text: "Success! Chat created."})"s).send();
}
int main()
{
    DatabaseManager::connect("mongodb://localhost:27017", "fireamp");
    fireampHttpServer.addRequestHandler("^/register$", handleRegisterEndpoint);
    fireampHttpServer.addRequestHandler("^/login$", handleLoginEndpoint);
    fireampHttpServer.addRequestHandler("^/createChat", handleChatCreateEndpoint);
    fireampHttpServer.addRequestHandler("^/sendMsg", handleLoginEndpoint);
    fireampHttpServer.addRequestHandler("^/$", [](const HTTPRequest& request) {
        using namespace std::string_literals;
        HTTPResponse(fireampHttpServer, request, {HTTP::FrameUtils::HTTPHeaderField{8}}, R"({code: 5, text: "Server error! Please report this to the owners of this instance"})"s).send();
    });
    fireampHttpServer.start();
    std::cin.get();
    fireampHttpServer.stop();
    return 0;
}
