/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef FIREAMPSERVER_FIREAMPHTTPERRORUTILS_H
#define FIREAMPSERVER_FIREAMPHTTPERRORUTILS_H

#include "vulcanohttp/HTTPServer.h"
namespace HTTPErrorUtils
{
    void sendMethodNotAllowed(HTTPServer& httpServer, const HTTPRequest& request,
                              const std::string& allowedMethods);
    void sendErrorResponse(HTTPServer& httpServer, const HTTPRequest& request,const std::string& responseCode,
                              const std::string& errorMessage);
};

#endif // FIREAMPSERVER_FIREAMPHTTPERRORUTILS_H
