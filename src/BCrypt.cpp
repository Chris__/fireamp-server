/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BCrypt.h"
#include <crypt.h>

std::string BCrypt::hashPassword(const std::string& pw)
{
    return crypt(pw.data(),crypt_gensalt("$2b$",13, nullptr, 0));
}
bool BCrypt::validatePassword(const std::string& pw,const std::string& hashedPW)
{
    return crypt(pw.data(),hashedPW.data()) == hashedPW;
}
