/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DatabaseManager.h"

mongocxx::client DatabaseManager::client;
mongocxx::database DatabaseManager::db;
mongocxx::collection DatabaseManager::Collections::accounts;
mongocxx::collection DatabaseManager::Collections::chats;

void DatabaseManager::connect(bsoncxx::string::view_or_value uri, bsoncxx::string::view_or_value database)
{

    client = std::move(mongocxx::client{mongocxx::uri{std::move(uri)}});
    db = std::move(client[std::move(database)]);
    Collections::accounts = std::move(db["accounts"]);
    Collections::chats = std::move(db["chats"]);
}

