/*
 * Copyright (C) 2021 Christian Nagel <chris.imx@online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FIREAMPSERVER_DATABASEMANAGER_H
#define FIREAMPSERVER_DATABASEMANAGER_H

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>


namespace DatabaseManager
{
    mongocxx::instance instance ();;

    extern mongocxx::client client;
    extern mongocxx::database db;

    namespace Collections
    {
        extern mongocxx::collection accounts;
        extern mongocxx::collection chats;
    }

    void connect(bsoncxx::string::view_or_value uri, bsoncxx::string::view_or_value database);

};

#endif // FIREAMPSERVER_DATABASEMANAGER_H
