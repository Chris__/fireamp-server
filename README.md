# Fireamp Messenger Server

## Overview

Fireamp is a free and open-source messenger. This is the server of it. It is written in C++ and builds with CMake.

## Supported Platforms

Currently, the server only supports linux.

## Building

Building is done with CMake, so make sure you have version 3.19 or higher installed. You also need OpenSSL. To build,
execute this from the root directory (the one with the "src" folder)

    mkdir build-output
    cmake . -B build-output
    cd build-output
    make

This builds the program for your platform. Then copy FireampServer and all .so files to a directory and ./FireampServer
starts the server.

## Dependencies

The server doesn't use a lot of dependencies: OpenSSL (TLS and cryptographic functions)
, [VulcanoHTTP](https://gitlab.com/Chris__/vulcanohttp) (HTTP/2 Server Library)
, [LiteLog++](https://gitlab.com/Chris__/litelogcpp)(Logger library)
VulcanoHTTP and LiteLog++ are automatically downloaded and built through CMake.

## FAQ and Troubleshooting

If you face any problems, please take a look at
the [FAQ & Troubleshooting-Guide](https://gitlab.com/Chris__/fireamp-server/-/wikis/FAQ/FAQ-&-Troubleshooting#building)

## Binaries

For each commit binaries are built with GitLab CI as artifacts.

## Development Process

- [ ] Fireamp Server
    - [ ] Server configuration
    - [x] Login and Registration
    - [ ] Account settings (changing password, username, delete account, resetting password)
    - [ ] Two-Factor authentication
    - [ ] Profile customization
    - [ ] Chats
    - [ ] Group Chats
    - [ ] Deleting and Editing Messages
    - [ ] Blocking
    - [ ] VoIP
    - [ ] Sending files
    - [ ] Direct sending of files over P2P, if other peer is online (mostly client work, but negotiation over server)

## Bugreports and Feature requests

Please submit an issue to this repo if you find any bugs or have an idea for a feature. If the bug is security-relevant,
please mark the issue as confidential. For more information read the [Contribution Guidelines](CONTRIBUTING.md).

## Contributions

Contributions are welcome, and it would be amazing if you want to help. Refer to
the [Contribution Guidelines](CONTRIBUTING.md) for more information.

## Contributors

All the contributors will be listed in the THANKS file.

## License

Fireamp Server is licensed under version 3 or later of the GPL license, which is a free-as-in-freedom copyleft license.
It allows you to do almost anything as long as you include the license and copyright notice and also license modified
distributions of this software under the same terms and with the same freedoms as the ones given by the GPL. You can
read the license [here](LICENSE).
